from random import randint
name = input("Hi! What is your name? ")

for x in range(1,6):
    print("Guess",x,":",name,"were you born in",
        randint(1,12),"/",randint(1924,2004),"?")
    answer = input("(yes/no): ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif answer == "no":
        if x == 5: break
        print("Drat! Lemme try again!")

print("I have other things to do. Good bye.")
exit()
